// SPDX-FileCopyrightText: Copyright (c) 2010-2024 Chris Pressey, Cat's Eye Technologies.
// This work is distributed under a 2-clause BSD license. For more information, see:
// SPDX-License-Identifier: LicenseRef-BSD-2-Clause-X-Whothm

package tc.catseye.whothm;

import java.util.Set;
import java.util.HashSet;

class TruthTable {
    Set<String> tt;

    TruthTable() {
        tt = new HashSet<String>();
    }
    
    void mapToTrue(String truthPair) {
        tt.add(truthPair);
    }
    
    boolean apply(boolean a, boolean b) {
        String target = (a ? "T" : "F") + (b ? "T" : "F");
        return tt.contains(target);
    }

    public String toString() {
        return "TruthTable(" + tt.toString() + ")";
    }

}
